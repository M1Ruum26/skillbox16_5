﻿#include <iostream>
#include <time.h>

int main()
{
    const int N = 5;
    int s, sum = 0;
    int array [N][N];

    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);
    s = buf.tm_mday % N;

    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            array[i][j] = i + j;
            std::cout << array[i][j] << ' ';
            if (i == s)
                sum += array[i][j];
        }
        std::cout << '\n';
    }
    std::cout << "sum = " << sum;
}